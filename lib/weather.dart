import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
void main() {
  runApp(WeatherPage());
}

class WeatherPage extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('อ.เมืองชลบุรี'
            , style: TextStyle(fontSize: 30),),
          centerTitle: true,
          backgroundColor: Colors.indigo,
        ),
        backgroundColor: Colors.lightBlueAccent,
        body: ListView(
          children: <Widget>[
            Container( // องศา
              //padding: EdgeInsets.all(15.0),
              width: double.infinity,
              child: Text(
                "31°", textAlign: TextAlign.center,
                style: TextStyle(fontSize: 90, color: Colors.white,),
              ),
            ),
        Container(
          width: double.infinity,
          child: Text(
            "เมฆเป็นบางส่วน", textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, color: Colors.white,),
          ),
        ),
        Container(
          width: double.infinity,
          child: Text(
            "สูงสุด: 32° ต่ำสุด: 20°", textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, color: Colors.white,),
          ),
        ),

            Container(
              height: 200,
              margin: EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.indigo,
              ),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(15.0),
                    child: Text('มีเมฆเป็นบางส่วนระหว่างเวลา 15:00-18:00 '
                        'และคาดว่ามีเมฆบางส่วนมากเวลา 18:00' ,
                        style: TextStyle(fontSize: 15, color: Colors.white,)),
                  ),
                  Divider(
                    color: Colors.white,
                    height: 10,
                  ),
                  Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      DegreeNow(),
                      Degree15(),
                      Degree16(),
                      Degree17(),
                      Degree18(),
                      Degree19(),
                      Degree20(),

                    ],
                  ),
                  )
                ],
              ),
            ),

            Container(
              height:400,
              margin: EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.indigo,
              ),
              child: Column(
                children: <Widget>[
                  Thu(),
                  Fri(),
                  Sat(),
                  Sun(),
                  Mon(),
                  Tue(),
                  Wed(),
                ],
              ),

            ),


          ],


        ),

        //เมนูด้านล่าง
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.indigo,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.map_outlined
                , color: Colors.white, size: 30,),
              label: '',

            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.location_on, color: Colors.white, size: 30,),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.table_rows, color: Colors.white, size: 30,),
              label: '',
            ),
          ],

        ),
      ),
    );
  }

  Widget Thu() {
    return ListTile(
      leading: Icon(Icons.cloud ,color: Colors.white),
      title:Text("วันนี้      20° --- 32° ",style: TextStyle(color: Colors.white,fontSize: 18),),

    );
  }
}
Widget Fri() {
  return ListTile(
    leading:Icon(Icons.cloud,color: Colors.white),
    title:Text("ศ.          20° --- 31° ",style: TextStyle(color: Colors.white,fontSize: 18 ),),

  );
}
Widget Sat() {
  return ListTile(
    title: Text("ส.          19° --- 30° ",style: TextStyle(color: Colors.white ,fontSize: 18),),
    leading: Icon(Icons.sunny,color: Colors.yellow),

  );
}
Widget Sun(){
  return ListTile(
    title: Text("อา.         18° --- 30° ",style: TextStyle(color: Colors.white ,fontSize: 18),),
    leading: Icon(Icons.cloud,color: Colors.white),
  );
}

Widget Mon(){
  return ListTile(
    title: Text("จ.           20° --- 30° ",style: TextStyle(color: Colors.white ,fontSize: 18),),
    leading: Icon(Icons.cloud,color: Colors.white),
  );
}

Widget Tue(){
  return ListTile(
    title: Text("อ.           19° ------------------------------- 31° ",style: TextStyle(color: Colors.white ,fontSize: 18),),
    leading: Icon(Icons.cloud,color: Colors.white),
  );
}
Widget Wed(){
  return ListTile(
    title: Text("พ.           22° ------------------------------ 31° ",style: TextStyle(color: Colors.white ,fontSize: 18),),
    leading: Icon(Icons.cloud,color: Colors.white),
  );
}

Widget DegreeNow(){
  return Column(
    children: <Widget>[
      Text("ตอนนี้",style: TextStyle(color:
      Colors.white),),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("31°",style: TextStyle(color:
      Colors.white),),

    ],
  );
}
Widget Degree15(){
  return Column(
    children: <Widget>[
      Text("15",style: TextStyle(color:
      Colors.white),),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("31°",style: TextStyle(color:
      Colors.white),),

    ],
  );
}

Widget Degree16(){
  return Column(
    children: <Widget>[
      Text("16",style: TextStyle(color:
      Colors.white),),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("31°",style: TextStyle(color:
      Colors.white),),

    ],
  );
}

Widget Degree17(){
  return Column(
    children: <Widget>[
      Text("17",style: TextStyle(color:
      Colors.white),),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("30°",style: TextStyle(color:
      Colors.white),),

    ],
  );
}
Widget Degree18(){
  return Column(
    children: <Widget>[
      Text("18",style: TextStyle(color:
      Colors.white),),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("27°",style: TextStyle(color:
      Colors.white),),

    ],
  );
}

Widget Degree19(){
  return Column(
    children: <Widget>[
      Text("19",style: TextStyle(color:
      Colors.white),),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("26°",style: TextStyle(color:
      Colors.white),),

    ],
  );
}

Widget Degree20(){
  return Column(
    children: <Widget>[
      Text("20",style: TextStyle(color:
      Colors.white),),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("25°",style: TextStyle(color:
      Colors.white),),

    ],
  );
}
